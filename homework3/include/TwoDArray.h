#ifndef TWODARRAY_H
#define TWODARRAY_H

#include <iostream>
#include <stdlib.h>
using namespace std;

class TwoDArray
{
    public:
        /* Overload prototipleri */
        int operator()(const int,const int) const;
        friend ostream& operator<<(ostream&, const TwoDArray&);
        friend istream& operator>>(istream&, const TwoDArray&);
        bool operator!=(const TwoDArray&) const;
        bool operator==(const TwoDArray&) const;
        void operator=(const TwoDArray&);

        /* Satır ve sütun boyutu */
        int getRows();
        int getColumns();

        TwoDArray(int,int);
        TwoDArray();
        virtual ~TwoDArray();
    protected:
    private:
        int *dizi;
        int x;
        int y;
};

#endif // TWODARRAY_H
