#include "TwoDArray.h"

TwoDArray::TwoDArray()
{
    x = 3;
    y = 3;

    dizi = new int[x*y];
    for(int i=0;i<(x*y);i++){
        dizi[i] = 0;
    }
}

TwoDArray::TwoDArray(int a, int b)
{
    x = a;
    y = b;

    dizi = new int[x*y];
    for(int i=0;i<(x*y);i++){
       dizi[i] = 0;
    }
}

int TwoDArray::getRows()
{
    return x;
}

int TwoDArray::getColumns()
{
    return y;
}

// TwoDArray& .. şeklinde overload edilseydi if(this!=&obj) yapılacak, *this return edilecekti.
int TwoDArray::operator()(const int a,const int b) const
{
    if(a<x && b<y){
        return dizi[y*a+b];
    }else{
        // Dönüş değeri int vs. olan fonksiyonlarda genelde cout yapılmaz, boyut aşımını belitmek için ekledim.
        cout << "Dizi boyutunu aştınız." << endl;
        return 0;
    }
}


ostream& operator<<(ostream& out, const TwoDArray& obj)
{
    cout << "--- Tüm Dizi Elemanları ---" << endl;
    int k=0;
    for(int i=0;i<obj.x;i++){
        for(int j=0;j<obj.y;j++){
            out << "NO: " << (k+1) << " | dizi[" << i << "][" << j << "]: " << obj.dizi[k] << endl;
            k++;
        }
    }
    return out;
}

istream& operator>>(istream& in, const TwoDArray& obj)
{
    int k=0;
    for(int i=0;i<obj.x;i++){
        for(int j=0;j<obj.y;j++){
            cout << "dizi[" << i << "][" << j << "]: ";
            in >> obj.dizi[k];
            k++;
        }
    }
    return in;
}

bool TwoDArray::operator!=(const TwoDArray& obj) const
{
    if(this->x == obj.x && this->y == obj.y){
        for(int i=0;i<(obj.x*obj.y);i++){
            if(this->dizi[i] != obj.dizi[i]){
                return true;
            }
        }
        return false;
    }else{
        return true;
    }
}

bool TwoDArray::operator==(const TwoDArray& obj) const
{
    if(this->x == obj.x && this->y == obj.y){
        for(int i=0;i<(obj.x*obj.y);i++){
            if(this->dizi[i] != obj.dizi[i]){
                return false;
            }
        }
    }else{
        return false;
    }
}

void TwoDArray::operator=(const TwoDArray& obj)
{
    x = obj.x;
    y = obj.y;
    delete[] dizi;
    dizi = new int[x*y];
    for(int i=0;i<(x*y);i++){
        dizi[i] = obj.dizi[i];
    }
}

TwoDArray::~TwoDArray()
{
    //test delete[] dizi;
}
