#include <iostream>
#include <TwoDArray.h>

using namespace std;

int main()
{
// Emre Ozel
// KTU-CENG #314037
// emreozel.net
    TwoDArray array1(2,4);
    TwoDArray array2(1,3);

    // Satır ve sütun boyutu fonksyionları
    /*
    cout << "Satır boyutu: " << array1.getRows();
    cout << " | Sütun boyutu: " << array1.getColumns() << endl;
    cout << "Satır boyutu: " << array2.getRows();
    cout << " | Sütun boyutu: " << array2.getColumns() << endl;
    */

    // Overload: >> Operatörü (Tüm Elemanları Kullanıcıdan Alma)
    //cout << endl;
    //cin >> array1;
    //cin >> array2;

    // Overload: << Operatörü (Tüm Elemanları Yazdırma)
    //cout << array1;
    //cout << array2;

    // Overload: != Operatörü
    /*
    if(array1 != array2){
        cout << "Diziler eşit değil." << endl;
    }else{
        cout << "Diziler eşit." << endl;
    }
    */

    // Overload: == Operatörü
    /*
    if(array1 == array2){
        cout << "Diziler eşit." << endl;
    }else{
        cout << "Diziler eşit değil." << endl;
    }
    */

    // Dizinin istediğimiz elemanına erişim
    //cin >> array1;
    //cout << "Erişilen Eleman: " << array1(1,1) << endl;


    // Overload: = Operatörü (Dizileri eşitleme)
    /*
    cin >> array1;
    cin >> array2;
    array1 = array2;
    cout << "Satır boyutu: " << array1.getRows();
    cout << " | Sütun boyutu: " << array1.getColumns() << endl;
    cout << "Satır boyutu: " << array2.getRows();
    cout << " | Sütun boyutu: " << array2.getColumns() << endl;
    cout << array1;
    */

    return 0;
}
